# Frontend Coding Assessment

## Overview

A farm of 30 solar panels requires a dashboard to monitor energy production. Display voltage and wattage per solar panel as well as the combined energy per whole farm. Dashboard should have a user-friendly design and be accessible via a web browser.

## Requirement Details

- Display the total energy in kWh produced by the farm
- Solar panels should be laid out in a grid where each cell has values of current voltage and wattage output.
- If the value for panel voltage is less than 10 volts and the value for wattage is less than 200 watts/hour, mark the panel with a red label (weak). Otherwise panels should be marked with green (healthy).
- Supply a summary of heathy/weak panels in the park overview section
- Update the UI with fresh data every 5 seconds
- The UI adapts to desktop, tablet and smartphone screens
- A 1280px-wide desktop screen fits the UI without need for scrolling
- Document your development with git
- Write tests where appropriate

## Resources

- Use provided files - `server.js` and `data.js`. They act as a simple API with updated timestamps and energy values. Install dependencies as appropriate.
- Use any other libraries/frameworks you see fit in building the UI
- Email your solution or repository link to rando@intertrust.com and feel free to ask any clarification along the way
